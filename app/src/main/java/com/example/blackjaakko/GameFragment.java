package com.example.blackjaakko;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class GameFragment extends Fragment {



    private OnDrawDeckListener mCallback1;
    private OnDrawCardListener mCallback2;
    private OnStartGameListener newGameCallback;

    public interface OnStartGameListener {
        void onStartGame(View view);
    }
    public interface OnDrawDeckListener {
        void onHold(View view);
    }

    public interface OnDrawCardListener {
        void onDrawCard(View view);
    }

    public GameFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_game, container, false);

        return v;
    }

    public void onHold(View view) {
        mCallback1.onHold(view);
    }

    public void onDrawCard(View view) {
        mCallback2.onDrawCard(view);
    }

    public void onStartGame(View view) { newGameCallback.onStartGame(view); }
}
