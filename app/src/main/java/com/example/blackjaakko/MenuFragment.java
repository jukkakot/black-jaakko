package com.example.blackjaakko;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class MenuFragment extends Fragment {

    private OnNewGameListener mCallback1;

    public interface OnNewGameListener {
        void onNewGame(View view);
    }


    public MenuFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menu, container, false);

        return v;
    }

    public void onNewGame(View view) {
        mCallback1.onNewGame(view);
    }

}
