package com.example.blackjaakko;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Implementation of headless Fragment that runs an AsyncTask to fetch data from the network.
 */
public class ApiRequestFragment extends Fragment {
    public static final String TAG = "ApiRequestFragment";
    private RequestCallback callback;
    private DownloadTask downloadTask;

    private ApiRequestFragment() {
    }

    public static ApiRequestFragment getInstance(FragmentManager fragmentManager) {
        ApiRequestFragment apiRequestFragment = (ApiRequestFragment) fragmentManager
                .findFragmentByTag(ApiRequestFragment.TAG);
        if (apiRequestFragment == null) {
            apiRequestFragment = new ApiRequestFragment();
            fragmentManager.beginTransaction().add(apiRequestFragment, TAG).commit();
        }
        return apiRequestFragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (RequestCallback) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    public void startDownload(String url,int type) {
        downloadTask = new DownloadTask(callback,type);
        downloadTask.execute(url);
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {

        private RequestCallback callback;
        private int callType;
        DownloadTask(RequestCallback callback,int callType) {
            setCallback(callback);
            this.callType = callType;
        }

        void setCallback(RequestCallback callback) {
            this.callback = callback;
        }



        public static final String REQUEST_METHOD = "GET";
        public static final int READ_TIMEOUT = 15000;
        public static final int CONNECTION_TIMEOUT = 15000;

        @Override
        protected String doInBackground(String... params) {
            String stringUrl = params[0];

            String result;
            String inputLine;

            try {
                URL myUrl = new URL(stringUrl);
                HttpURLConnection connection = (HttpURLConnection)
                        myUrl.openConnection();
                connection.setRequestMethod(REQUEST_METHOD);
                connection.setReadTimeout(READ_TIMEOUT);
                connection.setConnectTimeout(CONNECTION_TIMEOUT);

                connection.connect();
                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());
                BufferedReader reader = new BufferedReader(streamReader);
                StringBuilder stringBuilder = new StringBuilder();
                while ((inputLine = reader.readLine()) != null) {
                    stringBuilder.append(inputLine);
                }
                reader.close();
                streamReader.close();
                result = stringBuilder.toString();
            } catch (IOException e) {
                e.printStackTrace();
                result = null;
            }
            return result;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            callback.onRequestCompleted(result,callType);
        }

    }
}
