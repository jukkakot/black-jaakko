package com.example.blackjaakko;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.core.app.NavUtils;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import android.content.ClipData;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;


public class MainActivity extends AppCompatActivity implements RequestCallback,
        GameFragment.OnDrawDeckListener, GameFragment.OnDrawCardListener, MenuFragment.OnNewGameListener,
        GameFragment.OnStartGameListener {

    private final static int NEW_DECK = 0,DRAW_CARD =1;
    private ApiRequestFragment apiRequestFragment;
    private String deckId, imageUrl;

    private int playerCardAmount,dealerCardAmount = 0, playerAceAmount,
            dealerAceAmount = 0, playerTotal = 0, dealerTotal = 0;

    private StatsFragment statsFragment;

    boolean isOnHold = false;

    private TextView winner;
    private Button holdB,hitB;
 //  private StatsFragment statsFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        apiRequestFragment = ApiRequestFragment.getInstance(getSupportFragmentManager());

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.include, new MenuFragment());
        transaction.addToBackStack(null);
        transaction.commit();
        statsFragment = new StatsFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        ActionMenuItemView statsButton = (ActionMenuItemView) findViewById(R.id.action_stats);
        switch (item.getItemId()) {
            case android.R.id.home:
                getSupportFragmentManager().popBackStack();
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                return true;
            case R.id.action_stats:
                if(null == getSupportFragmentManager().findFragmentByTag("stats")){

                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.add(R.id.include, statsFragment,"stats");
                    transaction.addToBackStack(null);
                    transaction.commit();
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                }else {

                    getSupportFragmentManager().popBackStack();
                    if( getSupportFragmentManager().getBackStackEntryCount() != 2) {
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    }
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        //Tyhjä, jotta mitään ei tapahdu
    }

    @Override
    public String onRequestCompleted(String result,int type) {
        holdB = findViewById(R.id.holdButton);
        hitB = findViewById(R.id.hitButton);
        winner = findViewById(R.id.winnerText);
        JSONObject json;

        try {
            json = new JSONObject(result);
            switch(type){
                case (NEW_DECK):
                    deckId = json.get("deck_id").toString();
                    return null;
                case (DRAW_CARD):
                    JSONArray cards = json.getJSONArray("cards");
                    JSONObject card = cards.getJSONObject(0);
                    imageUrl =  card.getString("image");
                    String value = card.getString("value");
                    int intValue;
                    if(value.equals("JACK")|| value.equals("QUEEN") ||value.equals("KING")){
                        intValue = 10;
                    } else if (value.equals("ACE")){
                        intValue = 11;
                        if(isOnHold || playerCardAmount == 0){
                            dealerAceAmount++;
                        } else {
                            playerAceAmount++;
                        }
                    } else {
                        intValue = Integer.parseInt(value);
                        if(intValue == 0){
                            intValue = 10;
                        }
                    }

                    ImageView iv = new ImageView(getApplicationContext());
                    RelativeLayout ll;
                    /*
                    DIILERIN TOIMINNAT
                    */
                    if(isOnHold || dealerCardAmount == 0){
                        ll = (RelativeLayout) findViewById(R.id.dealerCards);
                        ll.addView(iv);
                        dealerCardAmount++;

                        dealerTotal += intValue;
                        while(dealerTotal >21 && dealerAceAmount !=0){
                            dealerTotal-=10;
                            dealerAceAmount--;
                        }
                        TextView tv = findViewById(R.id.dealerValue);
                        tv.setText(Integer.toString(dealerTotal));
                        iv.setPadding(37* dealerCardAmount,5* dealerCardAmount,0,0);

                        new DownloadImageTask(iv).execute(imageUrl);
                        if(dealerTotal <16 ){
                            if(dealerCardAmount != 1){
                                apiRequestFragment.startDownload("https://deckofcardsapi.com/api/deck/" + deckId + "/draw/?count=1", DRAW_CARD);
                            }
                        } else {
                            if(dealerTotal > 21 || dealerTotal < playerTotal){
                                if(playerTotal == 21 && playerCardAmount == 2){
                                    winner.setText("Black jaakko!");
                                    statsFragment.updateStats("bj");
                                    gameEnd();
                                } else {
                                    winner.setText("You won!");
                                    statsFragment.updateStats("win");
                                    gameEnd();
                                }
                            } else if(dealerTotal == playerTotal){
                                winner.setText("Draw!");
                                statsFragment.updateStats("draw");
                                gameEnd();
                            } else {
                                winner.setText("You lost!");
                                statsFragment.updateStats("lose");
                                gameEnd();
                            }
                        }
                    } else {
                        /*
                        PELAAJAN TOIMINNAT
                         */
                        ll = (RelativeLayout) findViewById(R.id.playerCards);
                        playerCardAmount++;
                        playerTotal += intValue;
                        while(playerTotal >21 && playerAceAmount !=0){
                            playerTotal-=10;
                            playerAceAmount--;
                        }
                        TextView tv = findViewById(R.id.value);
                        tv.setText(Integer.toString(playerTotal));
                        iv.setPadding(37* playerCardAmount,5* playerCardAmount,0,0);
                        ll.addView(iv);
                        new DownloadImageTask(iv).execute(imageUrl);
                        if(playerTotal >21){
                            winner.setText("You lost!");
                            statsFragment.updateStats("lose");
                            gameEnd();
                        }else if(playerTotal == 21){
                            isOnHold = true;
                            apiRequestFragment.startDownload("https://deckofcardsapi.com/api/deck/" + deckId + "/draw/?count=1", DRAW_CARD);
                           // gameEnd();
                        }
                    }
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void gameEnd() {

        hitB.setEnabled(false);
        holdB.setEnabled(false);
        apiRequestFragment.startDownload("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=6",NEW_DECK);
        Button startB = findViewById(R.id.restartButton);
        startB.setVisibility(View.VISIBLE);
        startB.setEnabled(true);
    }
    @Override
    public void onHold(View view) {
        isOnHold = true;
        holdB.setEnabled(false);
        hitB.setEnabled(false);
        apiRequestFragment
                .startDownload("https://deckofcardsapi.com/api/deck/" + deckId + "/draw/?count=1"
                , DRAW_CARD);
    }

    @Override
    public void onDrawCard(View view) {
        apiRequestFragment
                .startDownload("https://deckofcardsapi.com/api/deck/"+deckId+"/draw/?count=1"
                        ,DRAW_CARD);

    }

    @Override
    public void onNewGame(View view) { //Siirrytään menusta peliin
        apiRequestFragment
                .startDownload("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=6"
                        ,NEW_DECK);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.include, new GameFragment());
        transaction.addToBackStack(null);
        transaction.commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onStartGame(View view) { //Aloitetaan uusi peli pelifragmentista
        RelativeLayout dealerCards = (RelativeLayout) findViewById(R.id.dealerCards);
        RelativeLayout playerCards = (RelativeLayout) findViewById(R.id.playerCards);
        playerCards.removeAllViews();
        dealerCards.removeAllViews();
        ((TextView)findViewById(R.id.winnerText)).setText("  ");
        playerCardAmount = 0;
        dealerCardAmount = 0;
        playerAceAmount = 0;
        dealerAceAmount = 0;
        playerTotal = 0;
        dealerTotal= 0;
        isOnHold = false;
        Button startB = findViewById(R.id.restartButton);
        startB.setVisibility(View.INVISIBLE);

        holdB.setEnabled(true);
        hitB.setEnabled(true);
        apiRequestFragment.startDownload("https://deckofcardsapi.com/api/deck/"+deckId+"/draw/?count=1",DRAW_CARD);
        apiRequestFragment.startDownload("https://deckofcardsapi.com/api/deck/"+deckId+"/draw/?count=1",DRAW_CARD);
        apiRequestFragment.startDownload("https://deckofcardsapi.com/api/deck/"+deckId+"/draw/?count=1",DRAW_CARD);
    }




    class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
