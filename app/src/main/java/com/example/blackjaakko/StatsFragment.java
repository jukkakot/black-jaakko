package com.example.blackjaakko;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class StatsFragment extends Fragment {
    //Statseja
    private int playerWinCount = 0, playerLoseCount = 0,
            playerBlackJackCount = 0, drawCount = 0;
    private TextView statsText;
    public StatsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stats, container, false);
        // Inflate the layout for this
        statsText = (TextView)view.findViewById(R.id.statsText);
        setStats();
        return view;
    }
    @Override
    public void onViewCreated(View view,Bundle saveInstanceState){
        super.onViewCreated(view,saveInstanceState);
        statsText = (TextView)view.findViewById(R.id.statsText);
    }
    public void updateStats(String type) {
        switch (type) {
            case ("win"):
                playerWinCount++;
                break;
            case ("lose"):
                playerLoseCount++;
                break;
            case ("bj"):
                playerBlackJackCount++;
                playerWinCount++;
                break;
            case ("draw"):
                drawCount++;
                break;
        }
    }
    public void setStats(){
        double ratio = 100;
        if(playerLoseCount != 0){
            ratio = ((double)playerWinCount/((double)playerLoseCount+(double)playerWinCount) )*100.0;

            ratio = BigDecimal.valueOf(ratio).setScale(2, RoundingMode.HALF_UP).doubleValue();
        }
        statsText.setText(  "Wins: "+playerWinCount+"\n"+
                            "Losses: "+playerLoseCount+"\n"+
                            "Black jaakkos: "+playerBlackJackCount+"\n"+
                            "Draws: "+drawCount+"\n"+
                            "W/L ratio: "+ratio+"%");
    }
}
